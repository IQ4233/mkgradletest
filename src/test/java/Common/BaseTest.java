package Common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import Parameters.ConfigFileReader;

public class BaseTest {
    public WebDriver driver;

    public BaseTest() {
        this.setupDriver();
    }

    public void setupDriver() {
        ConfigFileReader configFileReader = new ConfigFileReader();

        // Set driver option and Initialize web driver
        System.out.println(System.getProperty("user.dir") + configFileReader.getDriverPath());
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + configFileReader.getDriverPath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        this.driver = new ChromeDriver(options);
        this.driver.manage().window().maximize();

        this.driver.get(configFileReader.getApplicationUrl());
    }

}
