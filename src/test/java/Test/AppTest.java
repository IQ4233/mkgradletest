package Test;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common.BaseTest;
import EmulateAction.JsActions;
import Pages.MainPage;
import Pages.ProductListPage;
import Parameters.DataProviderClass;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AppTest extends BaseTest {

    @BeforeTest(alwaysRun = true)
    public void setup() throws InterruptedException {
//        MainPage main_page = new MainPage(this.driver);
//        try {
//            main_page.cookieBtn().click();
//        }catch(Exception e) {}
//        try {
//            main_page.countryChangeModalCloseBtn().click();
//        }catch(Exception e) {}

    }

//  @Test(priority = 0, groups = {"First Test"})
//  public void addToCartAnyProduct() throws InterruptedException {
//	  MainPage main_page = new MainPage(this.driver);
////	  Thread.sleep(5000);
////	  main_page.cookieBtn().click();
////	  Thread.sleep(2000);
////
////	  main_page.countryChangeModalCloseBtn().click();
////
////
////	  main_page.productCategoryNav(0).click();
////	  Thread.sleep(10000);
////	  Actions action = new Actions(driver);
////	  action.moveByOffset(30, 30).click().build().perform();
//
////	  main_page.asideNav(0, 1).click();
//	  main_page.productCategoryNav(0).click();
//	  this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//	  Actions action = new Actions(this.driver);
//	  action.moveByOffset(30, 30).click().build().perform();
//	  main_page.productItem("four", 0).click();
//
//	  this.driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
//	  ProductDetail product_detail = new ProductDetail(this.driver);
//	  product_detail.size_btn(0).click();
//	  product_detail.add_bag_btn().click();
//  }

    @Test(priority = 0, dataProvider = "AFRICA", dataProviderClass = DataProviderClass.class, groups = {"Country Currency Check"})
    public void countryCurrencyCheck(List<String> country_li) throws InterruptedException {
        MainPage main_page = new MainPage(this.driver);
        JsActions js_actions = new JsActions(this.driver);
        for(int index = 0; index < country_li.size(); index++) {
            main_page.countryCurrencyModalBtn().click();
            js_actions.visibleElement(main_page.countryCurrencyItem(country_li.get(index))).click();
            this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            System.out.println(main_page.countryCurrencyModalBtn().getText());
            try {
                main_page.cookieBtn().click();
            }catch(Exception e) {}
            try {
                main_page.countryChangeModalCloseBtn().click();
            }catch(Exception e) {}
        }
    }

    @Test(priority = 1, groups = {"Country Language Options Check"})
    public void countryLanguageOptionCheck() throws InterruptedException {
        // get items that has two options for language
        MainPage main_page = new MainPage(this.driver);
        main_page.countryCurrencyModalBtn().click();
        JsActions js_actions = new JsActions(this.driver);
        List<WebElement> country_label_li = main_page.countryCurrencyLabelList();
        List<WebElement> country_li = main_page.countryCurrencyItemList();
        for(int index = 0; index < country_li.size(); index++) {
            if (country_li.get(index).findElements(By.cssSelector("li")).size() > 1) {
                System.out.print(country_label_li.get(index).getText() + " - ");
                js_actions.visibleElement(country_li.get(index));
                System.out.println(country_li.get(index).getText());
            }else {
                continue;
            }
        }
    }

    @Test(priority = 2, groups = {"Products Display Sort Order Check"})
    public void productsDisplaySortOrderCheck() {
        MainPage main_page = new MainPage(this.driver);
        ProductListPage plp = new ProductListPage(this.driver);
        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        main_page.productCategoryNav(0).click();
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        plp.sortBtn().click();
        this.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        plp.sortOptionItem("PRICE HIGH TO LOW").click();
    }

    @AfterTest(alwaysRun = true)
    public void deleteDriver() {
        this.driver.quit();
    }
}
