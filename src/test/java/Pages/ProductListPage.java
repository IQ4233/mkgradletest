package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductListPage {
    WebDriver driver;
    WebDriverWait wait;

    public ProductListPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement sortBtn() {
        return this.driver.findElement(By.cssSelector("div.sort-dropdown-inner-container button"));
    }

    public WebElement sortOptionItem(String title) {
        WebElement result = null;
        List<WebElement> sort_option_li = this.driver.findElements(By.cssSelector("div.sort-dropdown-inner-container ul li"));
        for(int index = 0; index < sort_option_li.size(); index++) {
            System.out.println(sort_option_li.get(index).getText());
            if ((sort_option_li.get(index).getText().toLowerCase()).contains(title.toLowerCase())) {
                result = sort_option_li.get(index);
            }else {
                continue;
            }
        }
        return result;
    }
}
