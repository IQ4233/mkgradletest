package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProductDetail {

    WebDriver driver;

    public ProductDetail(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement size_btn(int index) {
        WebElement size_btn_ul = this.driver.findElement(By.cssSelector("div.size-container ul"));
        List<WebElement> size_btn_li = size_btn_ul.findElements(By.cssSelector("li.facet-size-options"));
        return size_btn_li.get(index);
    }

    public WebElement add_bag_btn() {
        WebElement add_bag_btn = this.driver.findElement(By.cssSelector("div.add-to-cart-sticky button"));
        return add_bag_btn;
    }

}
