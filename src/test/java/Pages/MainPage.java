package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {
    WebDriver driver;
    WebDriverWait wait;

    public MainPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(this.driver, 5);
    }

    public WebElement cookieBtn() {
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#onetrust-accept-btn-handler")));
        return this.driver.findElement(By.cssSelector("#onetrust-accept-btn-handler"));
    }

    public WebElement authModalCloseBtn() {
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#overlay-container-id svg")));
        return this.driver.findElement(By.cssSelector("#overlay-container-id svg"));
    }

    public WebElement countryChangeModalCloseBtn() {
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.country-change-dialog button.close-button")));
        return this.driver.findElement(By.cssSelector("div.country-change-dialog button.close-button"));

    }

    public WebElement productCategoryNav(int index) {
        List<WebElement> nav = this.driver.findElements(By.cssSelector("#HeaderHambergerMenu > div.mk-nav.columns > nav > div > div > div.nav-menu-subwrapper > ul > li"));
        return nav.get(index);
    }

    public WebElement asideNav(int index, int sub_index) throws InterruptedException {
        List<WebElement> aside_nav_li = this.driver.findElements(By.cssSelector("aside div.plp-navigation div.navigation-panel ul.nav-category-list"));
        WebElement aside_nav = aside_nav_li.get(index);
        aside_nav.click();
        Thread.sleep(5000);
        System.out.println(aside_nav_li.size());
        List<WebElement> aside_sub_link_li = aside_nav.findElements(By.cssSelector("a"));
        return aside_sub_link_li.get(sub_index);
    }

    public WebElement productItem(String tile_type, int index) {
        List<WebElement> product_wrapper = this.driver.findElements(By.cssSelector("main ul.product-wrapper li.row-" + tile_type + "-tile"));
        List<WebElement> product_li = product_wrapper.get(index).findElements(By.cssSelector("div.product-tile"));
        return product_li.get(index);
    }

    //	country currency select modal
    public WebElement countryCurrencyModalBtn() {
        this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button.countrySelector")));
        return this.driver.findElement(By.cssSelector("button.countrySelector"));
    }

    public List<WebElement> countryCurrencyLabelList() {
        List<WebElement> country_currency_label_li = this.driver.findElements(By.cssSelector("li.country-selector-items"));
        return country_currency_label_li;
    }

    public List<WebElement> countryCurrencyItemList() {
        List<WebElement> country_currency_li = this.driver.findElements(By.cssSelector("div.language-container ul.multi-language-list.display-hide"));
        return country_currency_li;
    }

    public WebElement countryCurrencyItem(String country_name) {
        WebElement result = null;
        this.wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.cssSelector("div.country-selector-panel li.country-selector-items div.country-name")));
        List<WebElement> country_region = this.driver.findElements(By.cssSelector("div.country-selector-panel li.country-selector-items"));
        for(int index = 0; index < country_region.size(); index++) {
            String country_str = country_region.get(index).findElement(By.cssSelector("div.country-name")).getText();
            if (country_str.contains(country_name)) {
                result = country_region.get(index).findElement(By.cssSelector("li.multi-language-list-item"));
                break;
            }
        }
        return result;
    }
}
