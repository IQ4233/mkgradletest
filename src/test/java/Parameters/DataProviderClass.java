package Parameters;


import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.DataProvider;

public class DataProviderClass {
    @DataProvider(name="NORTH AMERICA")
    public Object[][] getNorthAmericaCountry() {
        List<String> north_america = new ArrayList<String>();
        north_america.add("Canada");
        north_america.add("United States");
        return new Object[][] {{ north_america }};
    }

    @DataProvider(name="MIDDLE EAST")
    public Object[][] getMiddleEastCountry() {
        List<String> middle_east = new ArrayList<String>();
        middle_east.add("Bahrain");
        middle_east.add("Israel");
        middle_east.add("Kuwait");
        middle_east.add("Lebanon");
        middle_east.add("Qatar");
        middle_east.add("Saudi Arabia");
        middle_east.add("United Arab Emirates");
        return new Object[][] {{ middle_east }};
    }

    @DataProvider(name="AFRICA")
    public Object[][] getAfricaCountry() {
        List<String> africa = new ArrayList<String>();
        africa.add("Egypt");
        africa.add("Ghana");
        africa.add("Nigeria");
        africa.add("South Africa");
        return new Object[][] {{ africa }};
    }

    @DataProvider(name="ASIA PACIFIC")
    public Object[][] getAsiaPacificCountry() {
        List<String> asia_pacific = new ArrayList<String>();
        asia_pacific.add("Australia");
        asia_pacific.add("Hong Kong, China");
        asia_pacific.add("India");
        asia_pacific.add("Indonesia");
        asia_pacific.add("Malaysia");
        asia_pacific.add("New Zealand");
        asia_pacific.add("Pakistan");
        asia_pacific.add("Singapore");
        asia_pacific.add("Taiwan, China");
        asia_pacific.add("Thailand");
        return new Object[][] {{ asia_pacific }};
    }

    @DataProvider(name="EUROPE")
    public Object[][] getEuropeCountry() {
        List<String> europe = new ArrayList<String>();
        europe.add("Austria");
        europe.add("Belgium");
        europe.add("Croatia");
        europe.add("Czech Republic");
        europe.add("Denmark");
        europe.add("Finland");
        europe.add("France");
        europe.add("Germany");
        europe.add("Greece");
        europe.add("Hungray");
        europe.add("Italy");
        europe.add("Latvia");
        europe.add("Lithuania");
        europe.add("Luxembourg");
        europe.add("Netherlands");
        europe.add("Norway");
        europe.add("Poland");
        europe.add("Portugal");
        europe.add("Republic of Ireland");
        europe.add("Romania");
        europe.add("Russia");
        europe.add("Slovak Republic");
        europe.add("Spain");
        europe.add("Sweden");
        europe.add("Switzerland");
        europe.add("Turkey");
        europe.add("Ukraine");
        europe.add("United Kingdom");
        return new Object[][] {{ europe }};
    }

}
